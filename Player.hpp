#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Deck.hpp"

#include <vector>
#include <string>

class Player
{
public:
	Player();
	~Player();
	void win();
	void loss();
	void addCard(struct Card card);
	int handValue();
	int splitHandValue();
	void printHand();
	void splitHand();
	void addSplitCard( struct Card card );	

	std::vector< struct Card > primHand;
	std::vector< struct Card > secHand;
	int handScore;
	int splitHandScore;
	int wins;
	int losses;
	bool split;
};




#endif
