#include "Player.hpp"
#include "Deck.hpp"

#include <iostream>
#include <string>
#include <vector>

Player::Player()
{
	int wins = 0;
	int losses = 0;
	handScore = 0;
	splitHandScore = 0;
	split = false;
}

Player::~Player()
{
	handScore = 0;	
	splitHandScore = 0;
	primHand.clear();
	secHand.clear();
	wins = 0;
	losses = 0;
}

void
Player::win()
{
	wins += 1;
	primHand.clear();
	secHand.clear();
	handScore = 0;
	splitHandScore = 0;
}

void
Player::loss()
{
	losses += 1;
	primHand.clear();
	secHand.clear();
	handScore = 0;
	splitHandScore = 0;
}

void
Player::addCard( struct Card card )
{
	primHand.push_back( card );
	if ( card.secondary_value == 1 && handScore + 11 > 21 ) handScore += 1;
	else handScore += card.primary_value;
	
}

int
Player::handValue()
{
	return handScore;
}

void
Player::printHand()
{
	if( split ){
		std::cout <<"Hand One:" << "\t\t" << "Hand Two:" << std::endl;
		for (int i = 0; i < primHand.size(); i++) std::cout << primHand[i].face << " of " << 
			primHand[i].suit << "\t\t" << 
				secHand[i].face << " of " << secHand[i].suit << std::endl;
		std::cout << std::endl;
	}
	else{
		std::cout << std::endl;
                for (int i = 0; i < primHand.size(); i++) std::cout << primHand[i].face << " of " << primHand[i].suit << std::endl;
                std::cout << std::endl;

	}
}

void 
Player::splitHand()
{
	if( primHand[0].face == primHand[1].face ){
		
		secHand.push_back( primHand[1] );
		primHand.pop_back();
		splitHandScore = secHand[0].primary_value;
		handScore /= 2;	
		split = true;
	}
}

int 
Player::splitHandValue()
{
	return splitHandScore;
}

void
Player::addSplitCard( struct Card card )
{
 secHand.push_back( card );
        if ( card.secondary_value == 1 && handScore + 11 > 21 ) splitHandScore += 1;
        else splitHandScore += card.primary_value;

}
