#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <cstdlib>

#include "Deck.hpp"

Deck::Deck()//populates a deck of cards
{
	std::vector< std::string > suits;
	std::vector< std::string > faces;

	suits.push_back( "HEARTS" );
	suits.push_back( "DIAMONDS" ); 
	suits.push_back( "SPADES" );
	suits.push_back( "CLUBS" );

	faces.push_back( "ONE" );
	faces.push_back( "TWO" );
	faces.push_back( "THREE" );
	faces.push_back( "FOUR" );
	faces.push_back( "FIVE" );
	faces.push_back( "SIX" );
	faces.push_back( "SEVEN" );
	faces.push_back( "EIGHT" );
	faces.push_back( "NINE" );
	faces.push_back( "JACK" );
	faces.push_back( "QUEEN" );
	faces.push_back( "KING" );
	faces.push_back( "ACE" );
	
	for (int i = 0; i < suits.size(); i++){
		for (int j = 0; j < 13; j++){
			struct Card tempcard;
			if (j < 9){
				tempcard.primary_value = j+1;
				tempcard.secondary_value = -1;
				tempcard.face = faces[j];
				tempcard.suit = suits[i];
				deck.push_back( tempcard );
			}
			else{
				if ( faces[j] == "JACK" ){
                                	tempcard.primary_value = 10;
					tempcard.secondary_value = -1;
                	                tempcard.face = faces[j];
                        	        tempcard.suit = suits[i];
                                	deck.push_back( tempcard );
                        	}
				else if ( faces[j] == "QUEEN" ){
                                        tempcard.primary_value = 10;
                                        tempcard.secondary_value = -1;
                                        tempcard.face = faces[j];
                                        tempcard.suit = suits[i];
                                        deck.push_back( tempcard );
                        	}

                        	else if ( faces[j] == "KING" ){
                                        tempcard.primary_value = 10;
                                        tempcard.secondary_value = -1;
                                        tempcard.face = faces[j];
                                        tempcard.suit = suits[i];
                                        deck.push_back( tempcard );
                        	}

                        	else if (faces[j] == "ACE"){
                                        tempcard.primary_value = 11;
                                        tempcard.secondary_value = 1;
                                        tempcard.face = faces[j];
                                        tempcard.suit = suits[i];
                                        deck.push_back( tempcard );
                        	}
                        	else std::cout << "What the hell is goin on ??????????\n";
				}

		}
	}
}//end Deck() constructor

void
Deck::shuffle()
{
	for( int i = 0; i < (rand()%50); i++) std::random_shuffle ( deck.begin(), deck.end() );
}

struct
Card
Deck::drawCard()
{
	struct Card temp = deck[deck.size()-1];
	deck.pop_back();
	return temp;
}

void
Deck::newDeck()
{
	deck.clear();
}

void
Deck::printDeck()
{
	std::cout << "deck size: " << deck.size() << "\n";
	for (int i = 0; i < deck.size(); i++){
		std::cout << "card #" << i+1 << " with value: " << deck[i].primary_value
			 << ", face: " << deck[i].face << ", and suit: " << deck[i].suit << "\n";
	}


}
