#include <iostream>
#include <vector>
#include <string>

#include "Deck.hpp"
#include "Player.hpp"

int 
main()
{
	std::cout << "welcome to blackjack! Let's get started...\n";
	Deck deck;
	Player player;
	Player dealer;
	bool game_in_session = true;
	
	int begin = 0;
	int hit = 1;
	int stay = 2;
	int end = 3;
	int option = begin;
	
	while( game_in_session ){

			if( option == begin ){
			////////////////BEGIN/////////////////////////////
				deck.newDeck();
				deck = Deck();
				deck.shuffle();

				player.addCard( deck.drawCard() );
				player.addCard( deck.drawCard() );
				dealer.addCard( deck.drawCard() );
				dealer.addCard( deck.drawCard() );				
				player.splitHand();
				//dealer.splithand();
		
				std::cout << "Here is your hand:\n";
				player.printHand(); 
				std::cout  <<"\nYour total hand is: ";
				player.printHand();
				
				std::string input;
				if ( player.handValue() == 21 || ( player.split && player.splitHandValue() == 21)) option = stay;
				else if ( player.handValue() > 21 && ( !player.split || player.splitHandValue() > 21 ) ){
					std::cout << "YOU LOSE!\nWould You Like To Play Again? ";
					std::cin >> input;
					player.loss();
					dealer.win();
					if( input == "yes" || input == "y") option = begin;
					else option = end;
				}
				else{
					std::cout << "\nWould you like to hit or stay? ";
					std::cin >> input;
					if ( input == "hit" ) option = hit;
					else option = stay;
				}

			}
			else if( option == hit ){
			///////////////////////////HIT/////////////////////////////////////
				if( player.split ) std::cout < "\nWhich hand would you like to hit?";
				int hand;
				std::cin >> hand;
				struct Card card = deck.drawCard();
				if ( hand == 2 ) player.addSplitCard( card );
				else player.addCard( card );	
				player.addCard( card );
				std::cout << "\nYou pulled a(n) " << card.face << " of " << card.suit << std::endl;
				std::cout << "\nYour current standing: ";
				player.printHand();
				std::string input;
				
				if ( player.handValue() == 21 || ( player.split && player.splitHandValue() == 21 ) ) option = stay;
                                else if ( player.handValue() > 21 && ( !player.split || player.splitHandValue() > 21 ) ){
                                        std::cout << "YOU LOSE!!!\nWould You Like To Play Again? ";
					std::cin >> input;
					player.loss();
					dealer.win();
					if( input == "yes" ) option = begin;
					else option = end;
                                }
                                else{
                                        std::cout << "Would you like to hit or stay? ";
                                        std::cin >> input;
                                        if ( input == "hit" ) option = hit;
                                        else option = stay;
                                }
			}
			else if( option == stay ){
			//////////////////////////////STAY//////////////////////////////
				while( dealer.handValue() < 16 || dealer.handValue() < player.handValue() || dealer.handValue() < player.splitHandValue() ){
					dealer.addCard( deck.drawCard() );
				}
				std::string input;

				std::cout << "\nYour Hand: " << player.handValue() << "\t\tDealer's Hand: " << dealer.handValue();
				if( dealer.handValue() <= 21 && dealer.handValue() >= player.handValue() && ( player.split && player.splitHandValue() <= dealer.handValue() ) ){
					std::cout << "\nYou Lose!\nWould You Like To Play Again? ";
					std::cin >> input;
					player.loss();
					dealer.win();
					if( input == "yes" || input == "y" ) option = begin;
					else option = end;
				} 
				else{
					std::cout << "\nYou Win!\nWould You Like To Play Again?";
					std::cin >> input;
					player.win();
					dealer.loss();
					if( input == "yes" || input == "y" ) option = begin;
					else option = end;
				}
			}

			else{
			//////////////////////////////////END//////////////////////////////
				std::cout << "That's all folks..\n";				
				game_in_session = false;

			}
	
	}


	std::cout << "Your winnigs:\nWins: " << player.wins << " Losses: " << player.losses << "\n";
	if( dealer.wins > player.wins ) std::cout << "Dealer Wins!\n";
	else "You Win!\n";
	return 0;
}
