/*
This class contains the data structures to represent all the 
cards of a deck and different functions to manipulate
the order of these structures
*/

#ifndef DECK_HPP
#define DECK_HPP

#include <vector>
#include <string>

struct Card
{
	int primary_value;
	int secondary_value;
	std::string face;
	std::string suit;
};

class Deck
{

public:
	Deck();
	
	void shuffle();
	struct Card drawCard();
	void newDeck();
	void printDeck();
	
	std::vector<struct Card> deck;

};

#endif //DECK_HPP 
